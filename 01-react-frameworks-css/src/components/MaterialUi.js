import React from "react";
import MaterialCard from "./MaterialCard";
import MaterialDrawer from "./MaterialDrawer";

const MaterialUi = () => {
    return (
        <>
            <h2>Material UI</h2>
            <MaterialCard />
            <MaterialDrawer />
        </>
    );
};

export default MaterialUi;
