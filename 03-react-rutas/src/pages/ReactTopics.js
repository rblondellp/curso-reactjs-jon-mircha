import React from "react";
import { Link, Routes, Route, useParams } from "react-router-dom";

const Topic = () => {
    let { topic } = useParams();

    return (
        <div>
            <h4>{topic}</h4>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Molestias aut beatae enim corrupti commodi iure, adipisci,
                minima aliquam dolor temporibus quas, sed voluptatem facilis!
                Mollitia recusandae ratione odio itaque laboriosam?
            </p>
        </div>
    );
};

const ReactTopics = () => {
    return (
        <div>
            <h3>Temas de React</h3>

            <ul>
                <li>
                    <Link to="jsx">JSX</Link>
                </li>
                <li>
                    <Link to="props">Props</Link>
                </li>
                <li>
                    <Link to="estado">Estado</Link>
                </li>
                <li>
                    <Link to="componentes">Componentes</Link>
                </li>
            </ul>

            <Routes>
                <Route path=":topic" Component={Topic} />
            </Routes>
        </div>
    );
};

export default ReactTopics;
