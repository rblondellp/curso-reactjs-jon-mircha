import ConceptosBasicos from "./components/ConceptosBasicos";

function App() {
    return (
        <div className="App">
            <h1>React Router</h1>
            <a
                href="https://reactrouter.com/en/main"
                target="_blanc"
                rel="noreferrer"
            >
                Documentación
            </a>

            <hr />

            <ConceptosBasicos />
        </div>
    );
}

export default App;
