import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <>
            <h3>Home</h3>
            <p>Bienvenido al tema de las rutas en react</p>

            <nav>
                <Link to="/about">About</Link>
                <Link to="/contact">Contact</Link>
            </nav>
        </>
    );
};

export default Home;
