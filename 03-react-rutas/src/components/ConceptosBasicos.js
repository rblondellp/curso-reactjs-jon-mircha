import React from "react";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Navigate,
} from "react-router-dom";
import About from "../pages/About";
import Contact from "../pages/Contact";
import Home from "../pages/Home";
import Error404 from "../pages/Error404";
import MenuConceptos from "./MenuConceptos";
import User from "../pages/User";
import Products from "../pages/Products";
import ReactTopics from "../pages/ReactTopics";
import Login from "../pages/Login";
import Dashboard from "../pages/Dashboard";
import PrivateRoute from "./PrivateRoute";

const ConceptosBasicos = () => {
    let auth;
    auth = true;

    return (
        <div>
            <h2>Conceptos Básicos</h2>
            <Router>
                <MenuConceptos />
                <Routes>
                    <Route path="/" Component={Home} />
                    <Route path="/about" Component={About} />
                    <Route path="/contact" Component={Contact} />
                    <Route path="/user/:username" Component={User} />
                    <Route path="/products" Component={Products} />
                    <Route path="/acerca" element={<Navigate to="/about" />} />
                    <Route
                        path="/contacto"
                        element={<Navigate to="/contact" />}
                    />
                    <Route path="/react/*" Component={ReactTopics} />
                    <Route path="/login" Component={Login} />
                    <Route
                        path="/dashboard"
                        element={
                            <PrivateRoute component={Dashboard} auth={auth} />
                        }
                    />

                    <Route path="*" Component={Error404} />
                </Routes>
            </Router>
        </div>
    );
};

// const ConceptosBasicos = () => {
//     return (
//         <div>
//             <h2>Conceptos Básicos</h2>
//             <Router>
//                 <Routes>
//                     <Route path="/" element={<Home />} />
//                     <Route path="/about" element={<About />} />
//                     <Route path="/contact" Component={Contact} />
//                     <Route
//                         path="/contact"
//                         element={
//                             <>
//                                 <Contact />
//                                 <p>
//                                     Lorem ipsum dolor sit amet consectetur
//                                     adipisicing elit. Itaque et corporis illum
//                                     iusto consectetur. Odio ratione minima
//                                     repellendus pariatur fugit impedit
//                                     cupiditate sed fuga. Porro vel velit
//                                     voluptatum tempora illum!
//                                 </p>
//                             </>
//                         }
//                     />
//                 </Routes>
//             </Router>
//         </div>
//     );
// };

export default ConceptosBasicos;
