import React from "react";
import { Link, NavLink } from "react-router-dom";

const MenuConceptos = () => {
    return (
        <nav>
            <ol>
                <li>
                    <span>Enlaces HTML (no recomendado)</span>

                    <a href="/">Home</a>
                    <a href="/about">About</a>
                    <a href="/contact">Contact</a>
                </li>

                <li>
                    <span>Componente Link: </span>

                    <Link to="/">Home</Link>
                    <Link to="/about">About</Link>
                    <Link to="/contact">Contact</Link>
                    <Link to="/not-found">Error 404</Link>
                </li>

                <li>
                    <span>Componente NavLink</span>

                    <NavLink to="/" activeclassname="active">
                        Home
                    </NavLink>
                    <NavLink to="/about" activeclassname="active">
                        About
                    </NavLink>
                    <NavLink to="/contact" activeclassname="active">
                        Contact
                    </NavLink>
                </li>

                <li>
                    <span>Parámetros</span>

                    <Link to="/user/rblondell">rblondell</Link>
                    <Link to="/user/jehernandez">jehernandez</Link>
                </li>

                <li>
                    <span>Parámetros de consulta:</span>

                    <Link to="/products">Productos</Link>
                </li>

                <li>
                    <span>Redirecciones:</span>

                    <Link to="/acerca">About</Link>
                    <Link to="/contacto">Contact</Link>
                </li>

                <li>
                    <span>Rutas anidadas:</span>

                    <Link to="/react">React</Link>
                </li>

                <li>
                    <span>Rutas privadas:</span>

                    <Link to="/login">Login</Link>
                    <Link to="/dashboard">Dashboard</Link>
                </li>
            </ol>
        </nav>
    );
};

export default MenuConceptos;
