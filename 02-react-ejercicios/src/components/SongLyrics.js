import React from "react";

const SongLyrics = ({ track }) => {
    return (
        <div>
            <h3>{track.name}</h3>
            <blockquote style={{ whiteSpace: "pre-wrap" }}>
                {track.wiki ? track.wiki.summary : track.url}
            </blockquote>

            {track.album && (
                <div>
                    <h3>Album</h3>
                    <div style={{ marginLeft: "40px" }}>
                        <h4>{track.album.title}</h4>
                        <img src={track.album.image[3]["#text"]} />
                    </div>
                </div>
            )}
        </div>
    );
};

export default SongLyrics;
