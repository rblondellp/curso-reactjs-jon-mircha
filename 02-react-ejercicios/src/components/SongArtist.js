import React from "react";

const SongArtist = ({ artist }) => {
    return (
        <section>
            <h3>{artist.name}</h3>
            <div style={{ marginLeft: "40px" }}>
                <img src={artist.image[3]["#text"]} alt={artist.name} />
                <p>{artist.tags.tag.map(el => el.name).join(", ")}</p>
                <a href={artist.url} target="_blanc" rel="noreferrer">
                    sitio web
                </a>
                <p style={{ whiteSpace: "pre-wrap" }}>{artist.bio.summary}</p>
            </div>
        </section>
    );
};

export default SongArtist;
