import React, { useState, useEffect } from "react";
import SongForm from "./SongForm";
import SongDetails from "./SongDetails";
import Loader from "./Loader";
import { helpHttp } from "../helpers/helpHttp";

const SongSearch = () => {
    const [search, setSearch] = useState(null);
    const [lyric, setLyric] = useState(null);
    const [bio, setBio] = useState(null);
    const [loading, setLoading] = useState(false);

    const apiKey = "b45b29d2b366f48af39f641a4bb61e44";

    useEffect(() => {
        if (search === null) return;

        const fetchData = async () => {
            const { artist, song } = search;

            let artistUrl = `http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=${artist}&api_key=${apiKey}&format=json`;
            let songUrl = `http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=${apiKey}&artist=${artist}&track=${song}&format=json`;

            // console.log(artistUrl, songUrl);

            setLoading(true);

            const [artistRes, songRes] = await Promise.all([
                helpHttp().get(artistUrl),
                helpHttp().get(songUrl),
            ]);

            // console.log(artistRes, songRes);

            setBio(artistRes);
            setLyric(songRes);
            setLoading(false);
        };

        fetchData();
    }, [search]);

    const handleSearch = data => {
        // console.log(data);
        setSearch(data);
    };

    return (
        <div>
            <h2>Song Search</h2>
            <article className="grid-1-3">
                <SongForm handleSearch={handleSearch} />
                {loading && <Loader />}
                {search && !loading && (
                    <SongDetails search={search} lyric={lyric} bio={bio} />
                )}
            </article>
        </div>
    );
};

export default SongSearch;
