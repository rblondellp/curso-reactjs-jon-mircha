import React from "react";
import SongArtist from "./SongArtist";
import SongLyrics from "./SongLyrics";
import Message from "./Message";

const SongDetails = ({ search, lyric, bio }) => {
    if (!lyric || !bio) return null;

    return (
        <>
            {lyric.error || lyric.name === "AbortError" ? (
                <Message
                    msg={`Error: no existe la canción "<em>${search.song}</em>"`}
                    bgColor="#dc3545"
                />
            ) : (
                <SongLyrics track={lyric.track} />
            )}

            {bio.artist ? (
                <SongArtist artist={bio.artist} />
            ) : (
                <Message
                    msg={`Error: no existe el intérprete "<em>${search.artist}</em>"`}
                    bgColor="#dc3545"
                />
            )}
        </>
    );
};

export default SongDetails;
